
#include "CaloDetDescr/CaloDepthTool.h"
#include "CaloDetDescr/CaloAlignTool.h"
#include "CaloDetDescr/CaloSuperCellAlignTool.h"
#include "../CaloSuperCellIDTool.h"
#include "../CaloTowerGeometryCondAlg.h"

DECLARE_COMPONENT( CaloDepthTool )
DECLARE_COMPONENT( CaloAlignTool )
DECLARE_COMPONENT( CaloSuperCellAlignTool )
DECLARE_COMPONENT( CaloSuperCellIDTool )
DECLARE_COMPONENT( CaloTowerGeometryCondAlg )

