/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/
#ifndef HLTSeeding_TrigIdentifiers_h
#define HLTSeeding_TrigIdentifiers_h

typedef int TriggerElementID;
typedef int ChainID;

#endif
