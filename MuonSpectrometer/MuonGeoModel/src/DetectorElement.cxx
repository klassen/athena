/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

#include "MuonGeoModel/DetectorElement.h"

namespace MuonGM {

    const StoredMaterialManager *DetectorElement::s_matManager = nullptr;

} // namespace MuonGM
