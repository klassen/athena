/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

#ifndef _ZDC_SINC_INTERP_
#define _ZDC_SINC_INTERP_

namespace ZDC
{
double SincInterp(const double* xvec, const double* pvec);   
}
#endif
